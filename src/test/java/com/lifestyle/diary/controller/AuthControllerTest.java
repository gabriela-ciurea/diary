package com.lifestyle.diary.controller;

import com.lifestyle.diary.dto.LoginDTO;
import com.lifestyle.diary.service.UserAccountService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class AuthControllerTest extends ControllerBase {

  @MockBean private UserAccountService userAccountService;
  private AuthController authController;

  private static final String USERNAME = "adamsmith";
  private static final String PASSWORD = "adam123";

  @Before
  public void setUp() {
    authController = new AuthController(userAccountService);
  }

  @Test
  public void testAuthenticateUserWithValidData() {
    LoginDTO loginDTO = new LoginDTO(USERNAME, PASSWORD);

    when(userAccountService.authenticate(any(LoginDTO.class))).thenReturn("token");
    ResponseEntity response = authController.authenticateUser(loginDTO);

    assertEquals(HttpStatus.OK.value(), response.getStatusCodeValue());
    assertNotNull(response.getHeaders().get(HttpHeaders.AUTHORIZATION));
  }
}
