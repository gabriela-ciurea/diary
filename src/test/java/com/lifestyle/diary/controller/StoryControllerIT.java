package com.lifestyle.diary.controller;

import com.lifestyle.diary.dto.StoryContentDTO;
import com.lifestyle.diary.dto.StoryDTO;
import com.lifestyle.diary.dto.StoryListDTO;
import com.lifestyle.diary.model.Story;
import com.lifestyle.diary.repository.StoryRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class StoryControllerIT extends ControllerBaseIT {
  @Autowired private StoryRepository storyRepository;
  private final String STORY_TITLE = "Story Title";
  private final String STORY_CONTENT = "Story content";
  private final Long INVALID_USER_ID = 99999L;
  private final Long INVALID_STORY_ID = 99999L;

  @Before
  public void before() {
    super.setup();
  }

  @After
  public void after() {
    super.deleteAllTestUsers();
  }

  @Test
  public void testCreateValidStory() {
    StoryContentDTO storyContentDTO = new StoryContentDTO(STORY_TITLE, STORY_CONTENT);

    HttpEntity<StoryContentDTO> request =
        new HttpEntity<>(storyContentDTO, getAuthorizationHeaders());
    ResponseEntity<String> response =
        restTemplate.postForEntity(createStoriesUri(getTestUser().getId()), request, String.class);

    List<Story> stories = storyRepository.findByUserAccountId(getTestUser().getId());

    assertEquals(HttpStatus.CREATED.value(), response.getStatusCodeValue());
    assertEquals(1, stories.size());
    assertEquals(STORY_TITLE, stories.get(0).getTitle());
    assertEquals(STORY_CONTENT, stories.get(0).getContent());
  }

  @Test
  public void testCreateUnauthorized() {
    StoryContentDTO storyContentDTO = new StoryContentDTO(STORY_TITLE, STORY_CONTENT);

    HttpEntity<StoryContentDTO> request =
        new HttpEntity<>(storyContentDTO, getAuthorizationHeaders());
    ResponseEntity<String> response =
        restTemplate.postForEntity(createStoriesUri(INVALID_USER_ID), request, String.class);

    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getStatusCodeValue());
  }

  @Test
  public void testCreateWithoutAuthorizationHeaders() {
    StoryContentDTO storyContentDTO = new StoryContentDTO(STORY_TITLE, STORY_CONTENT);

    HttpEntity<StoryContentDTO> request = new HttpEntity<>(storyContentDTO);
    ResponseEntity<String> response =
        restTemplate.postForEntity(createStoriesUri(getTestUser().getId()), request, String.class);

    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getStatusCodeValue());
  }

  @Test
  public void testGetStorySuccessful() {
    createStory();
    HttpEntity request = new HttpEntity(getAuthorizationHeaders());
    List<Story> stories = storyRepository.findByUserAccountId(getTestUser().getId());

    ResponseEntity<StoryDTO> response =
        restTemplate.exchange(
            createStoriesUri(getTestUser().getId(), stories.get(0).getId()),
            HttpMethod.GET,
            request,
            StoryDTO.class);

    assertEquals(HttpStatus.OK.value(), response.getStatusCodeValue());
    assertEquals("Story Title", response.getBody().getTitle());
    assertEquals(stories.get(0).getId().longValue(), response.getBody().getId().longValue());
  }

  @Test
  public void testGetStoryUnauthorized() {
    Story story = createStory();
    HttpEntity request = new HttpEntity(getAuthorizationHeaders());
    ResponseEntity<StoryDTO> response =
        restTemplate.exchange(
            createStoriesUri(INVALID_USER_ID, story.getId()),
            HttpMethod.GET,
            request,
            StoryDTO.class);

    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getStatusCodeValue());
  }

  @Test
  public void testGetStoryNotFound() {
    HttpEntity request = new HttpEntity(getAuthorizationHeaders());
    ResponseEntity<StoryDTO> response =
        restTemplate.exchange(
            createStoriesUri(getTestUser().getId(), INVALID_STORY_ID),
            HttpMethod.GET,
            request,
            StoryDTO.class);

    assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCodeValue());
  }

  @Test
  public void testGetStoryListNotEmpty() {
    final int NUMBER_OF_STORIES = 3;
    createStories(NUMBER_OF_STORIES);

    HttpEntity request = new HttpEntity(getAuthorizationHeaders());
    ResponseEntity<StoryListDTO> response =
        restTemplate.exchange(
            createStoriesUri(getTestUser().getId()), HttpMethod.GET, request, StoryListDTO.class);

    assertEquals(HttpStatus.OK.value(), response.getStatusCodeValue());
    assertEquals(NUMBER_OF_STORIES, response.getBody().getStories().size());
  }

  @Test
  public void testGetStoryListEmpty() {
    HttpEntity request = new HttpEntity(getAuthorizationHeaders());
    ResponseEntity<StoryListDTO> response =
        restTemplate.exchange(
            createStoriesUri(getTestUser().getId()), HttpMethod.GET, request, StoryListDTO.class);

    assertEquals(HttpStatus.OK.value(), response.getStatusCodeValue());
    assertEquals(0, response.getBody().getStories().size());
  }

  @Test
  public void testGetStoryListUnauthorizedEvent() {
    HttpEntity request = new HttpEntity(getAuthorizationHeaders());
    ResponseEntity<StoryListDTO> response =
        restTemplate.exchange(
            createStoriesUri(INVALID_USER_ID), HttpMethod.GET, request, StoryListDTO.class);

    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getStatusCodeValue());
  }

  @Test
  public void testDeleteStorySuccessful() {
    Story story = createStory();

    HttpEntity request = new HttpEntity(getAuthorizationHeaders());
    ResponseEntity response =
        restTemplate.exchange(
            createStoriesUri(getTestUser().getId(), story.getId()),
            HttpMethod.DELETE,
            request,
            String.class);

    assertEquals(HttpStatus.OK.value(), response.getStatusCodeValue());
    assertFalse(storyRepository.findById(story.getId()).isPresent());
  }

  @Test
  public void testDeleteStoryUnauthorized() {
    Story story = createStory();

    HttpEntity request = new HttpEntity(getAuthorizationHeaders());
    ResponseEntity response =
        restTemplate.exchange(
            createStoriesUri(INVALID_USER_ID, story.getId()),
            HttpMethod.DELETE,
            request,
            String.class);

    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getStatusCodeValue());
  }

  @Test
  public void testDeleteStoryNotFound() {
    HttpEntity request = new HttpEntity(getAuthorizationHeaders());
    ResponseEntity response =
        restTemplate.exchange(
            createStoriesUri(getTestUser().getId(), INVALID_STORY_ID),
            HttpMethod.DELETE,
            request,
            String.class);

    assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCodeValue());
  }

  @Test
  public void testModifyStorySuccessful() {
    Story story = createStory();
    StoryContentDTO storyContentDTO = new StoryContentDTO(STORY_TITLE, STORY_CONTENT);

    HttpEntity<StoryContentDTO> request =
        new HttpEntity<>(storyContentDTO, getAuthorizationHeaders());

    ResponseEntity<StoryDTO> response =
        restTemplate.exchange(
            createStoriesUri(getTestUser().getId(), story.getId()),
            HttpMethod.PUT,
            request,
            StoryDTO.class);

    assertEquals(HttpStatus.OK.value(), response.getStatusCodeValue());
    assertEquals(story.getTitle(), response.getBody().getTitle());
    assertEquals(story.getContent(), response.getBody().getContent());
  }

  @Test
  public void testModifyStoryUnauthorized() {
    Story story = createStory();
    StoryContentDTO storyContentDTO = new StoryContentDTO(STORY_TITLE, STORY_CONTENT);

    HttpEntity<StoryContentDTO> request =
        new HttpEntity<>(storyContentDTO, getAuthorizationHeaders());

    ResponseEntity<StoryDTO> response =
        restTemplate.exchange(
            createStoriesUri(INVALID_USER_ID, story.getId()),
            HttpMethod.PUT,
            request,
            StoryDTO.class);

    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getStatusCodeValue());
  }

  @Test
  public void testModifyStoryNotFound() {
    StoryContentDTO storyContentDTO = new StoryContentDTO(STORY_TITLE, STORY_CONTENT);

    HttpEntity<StoryContentDTO> request =
        new HttpEntity<>(storyContentDTO, getAuthorizationHeaders());

    ResponseEntity<StoryDTO> response =
        restTemplate.exchange(
            createStoriesUri(getTestUser().getId(), INVALID_STORY_ID),
            HttpMethod.PUT,
            request,
            StoryDTO.class);

    assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCodeValue());
  }

  private Story createStory() {
    return storyRepository.save(
        new Story(LocalDateTime.now(), STORY_TITLE, STORY_CONTENT, getTestUser()));
  }

  private void createStories(int numberOfStories) {
    for (int i = 0; i < numberOfStories; i++) {
      createStory();
    }
  }

  private String createStoriesUri(Long userId) {
    return "/api/users/" + userId + "/stories";
  }

  private String createStoriesUri(Long userId, Long storyId) {
    return createStoriesUri(userId) + "/" + storyId;
  }
}
