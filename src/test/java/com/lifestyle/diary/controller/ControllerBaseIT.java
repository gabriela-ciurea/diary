package com.lifestyle.diary.controller;

import com.lifestyle.diary.dto.LoginDTO;
import com.lifestyle.diary.exception.UnauthorizedEventException;
import com.lifestyle.diary.model.Role;
import com.lifestyle.diary.model.RoleName;
import com.lifestyle.diary.model.UserAccount;
import com.lifestyle.diary.repository.RoleRepository;
import com.lifestyle.diary.repository.UserRepository;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.Optional;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class ControllerBaseIT {
  @Autowired protected TestRestTemplate restTemplate;
  @LocalServerPort protected int port;

  private UserAccount userAccount;
  @Autowired private UserRepository userRepository;
  @Autowired private PasswordEncoder passwordEncoder;
  @Autowired private RoleRepository roleRepository;
  private static final String SIGN_IN_URI = "/api/auth/signin";
  private static HttpHeaders authorizationHeaders;
  protected static final String TEST_USER_USERNAME = "test";
  protected static final String TEST_USER_EMAIL = "test@email.com";
  protected static final String TEST_USER_PASSWORD = "password";

  protected void setup() {
    this.userAccount = createTestUser(TEST_USER_USERNAME, TEST_USER_EMAIL);
    setupAuthorizationHeaders();
  }

  protected void deleteAllTestUsers() {
    userRepository.deleteAll();
  }

  protected UserAccount createTestUser(String username, String email) {
    if (userRepository.existsByUsername(username)) {
      return userRepository.findByUsernameOrEmail(username, email).orElse(null);
    } else {
      UserAccount userAccount =
          new UserAccount(
              "test", "test", username, email, passwordEncoder.encode(TEST_USER_PASSWORD));
      Role role =
          roleRepository
              .findByName(RoleName.ROLE_USER)
              .orElseGet(() -> roleRepository.save(new Role(RoleName.ROLE_USER)));

      userAccount.setRoles(Collections.singleton(role));
      return userRepository.save(userAccount);
    }
  }

  private void setupAuthorizationHeaders() {
    LoginDTO loginDTO = new LoginDTO(userAccount.getUsername(), TEST_USER_PASSWORD);

    ResponseEntity<String> response =
        restTemplate.exchange(
            createUrlWithPort(SIGN_IN_URI),
            HttpMethod.POST,
            new HttpEntity<>(loginDTO),
            String.class);

    String jwt =
        Optional.ofNullable(response.getHeaders().get(HttpHeaders.AUTHORIZATION))
            .map(a -> a.get(0))
            .orElse("");

    HttpHeaders headers = new HttpHeaders();
    headers.set(HttpHeaders.AUTHORIZATION, jwt);
    authorizationHeaders = headers;
  }

  protected UserAccount getTestUser() {
    if (userAccount == null) {
      throw new UnauthorizedEventException("Class not initialised");
    }
    return userAccount;
  }

  protected HttpHeaders getAuthorizationHeaders() {
    if (authorizationHeaders == null) {
      throw new UnauthorizedEventException("Class not initialised");
    }
    return authorizationHeaders;
  }

  protected String createUrlWithPort(String uri) {
    return "http://localhost:" + port + uri;
  }
}
