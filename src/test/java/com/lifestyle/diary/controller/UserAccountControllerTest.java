package com.lifestyle.diary.controller;

import com.lifestyle.diary.dto.CurrentUserInfoDTO;
import com.lifestyle.diary.dto.SignUpDTO;
import com.lifestyle.diary.exception.EmailAlreadyUsedException;
import com.lifestyle.diary.exception.ResourceNotFoundException;
import com.lifestyle.diary.exception.UnauthorizedEventException;
import com.lifestyle.diary.exception.UsernameAlreadyUsedException;
import com.lifestyle.diary.model.UserAccount;
import com.lifestyle.diary.service.UserAccountService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class UserAccountControllerTest extends ControllerBase {
  @MockBean private UserAccountService userAccountService;
  private UserAccountController userAccountController;

  private static final Long USER_ID = 1L;
  private static final String FIRST_NAME = "Adam";
  private static final String LAST_NAME = "Smith";
  private static final String USERNAME = "adamsmith";
  private static final String EMAIL = "adamsmith@email.com";
  private static final String PASSWORD = "adam123";
  private static final String JWT = "token";

  @Before
  public void setUp() {
    userAccountController = new UserAccountController(userAccountService);
  }

  @Test
  public void testRegisterUserWithValidData() {
    UserAccount userAccount = new UserAccount(FIRST_NAME, LAST_NAME, USERNAME, EMAIL, PASSWORD);
    SignUpDTO signUpDTO =
        new SignUpDTO(
            userAccount.getFirstName(),
            userAccount.getLastName(),
            userAccount.getUsername(),
            userAccount.getEmail(),
            userAccount.getPassword());

    when(userAccountService.registerUser(any(SignUpDTO.class))).thenReturn(userAccount);
    ResponseEntity response = userAccountController.registerUser(signUpDTO);

    assertEquals(HttpStatus.CREATED.value(), response.getStatusCodeValue());
  }

  @Test
  public void testRegisterUserWithUsernameTaken() {
    SignUpDTO signUpDTO = new SignUpDTO(FIRST_NAME, LAST_NAME, USERNAME, EMAIL, PASSWORD);

    when(userAccountService.registerUser(any(SignUpDTO.class)))
        .thenThrow(UsernameAlreadyUsedException.class);
    ResponseEntity response = userAccountController.registerUser(signUpDTO);

    assertEquals(HttpStatus.FORBIDDEN.value(), response.getStatusCodeValue());
  }

  @Test
  public void testRegisterUserWithEmailTaken() {
    SignUpDTO signUpDTO = new SignUpDTO(FIRST_NAME, LAST_NAME, USERNAME, EMAIL, PASSWORD);

    when(userAccountService.registerUser(any(SignUpDTO.class)))
        .thenThrow(EmailAlreadyUsedException.class);
    ResponseEntity response = userAccountController.registerUser(signUpDTO);

    assertEquals(HttpStatus.FORBIDDEN.value(), response.getStatusCodeValue());
  }

  @Test
  public void testGetCurrentUserInfoSuccessful() {
    CurrentUserInfoDTO currentUser =
        new CurrentUserInfoDTO(USER_ID, FIRST_NAME, LAST_NAME, USERNAME, EMAIL);

    when(userAccountService.getCurrentUserInfo(anyString())).thenReturn(currentUser);
    ResponseEntity<CurrentUserInfoDTO> response = userAccountController.getCurrentUserInfo(JWT);

    assertEquals(HttpStatus.OK.value(), response.getStatusCodeValue());
    assertEquals(USER_ID, response.getBody().getId());
    assertEquals(USERNAME, response.getBody().getUsername());
    assertEquals(EMAIL, response.getBody().getEmail());
  }

  @Test
  public void testGetCurrentUserInfoUnauthorized() {
    when(userAccountService.getCurrentUserInfo(anyString()))
        .thenThrow(new UnauthorizedEventException(""));

    ResponseEntity<CurrentUserInfoDTO> response = userAccountController.getCurrentUserInfo(JWT);
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getStatusCodeValue());
  }

  @Test
  public void testGetCurrentUserInfoNotFound() {
    when(userAccountService.getCurrentUserInfo(anyString()))
        .thenThrow(new ResourceNotFoundException(""));

    ResponseEntity<CurrentUserInfoDTO> response = userAccountController.getCurrentUserInfo(JWT);
    assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCodeValue());
  }
}
