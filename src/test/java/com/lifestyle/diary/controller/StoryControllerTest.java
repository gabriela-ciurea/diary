package com.lifestyle.diary.controller;

import com.lifestyle.diary.dto.StoryContentDTO;
import com.lifestyle.diary.dto.StoryDTO;
import com.lifestyle.diary.dto.StoryListDTO;
import com.lifestyle.diary.exception.ResourceNotFoundException;
import com.lifestyle.diary.exception.UnauthorizedEventException;
import com.lifestyle.diary.service.StoryService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class StoryControllerTest extends ControllerBase {
  @MockBean private StoryService storyService;
  private StoryController storyController;
  private static final Long USER_ID = 1L;
  private static final String JWT = "token";
  private static final Long STORY_ID = 1L;
  private static final Long INVALID_STORY_ID = 254L;
  private static final Long INVALID_USER_ID = 451L;

  @Before
  public void setup() {
    storyController = new StoryController(storyService);
  }

  @Test
  public void testCreateValidStory() {
    StoryContentDTO storyContentDTO = new StoryContentDTO();

    doNothing().when(storyService).create(any(StoryContentDTO.class), anyLong(), anyString());

    ResponseEntity response = storyController.create(storyContentDTO, USER_ID, JWT);
    assertEquals(HttpStatus.CREATED.value(), response.getStatusCodeValue());
  }

  @Test
  public void testCreateStoryUnauthorized() {
    StoryContentDTO storyContentDTO = new StoryContentDTO();
    doThrow(new UnauthorizedEventException(""))
        .when(storyService)
        .create(any(StoryContentDTO.class), anyLong(), anyString());

    ResponseEntity response = storyController.create(storyContentDTO, INVALID_USER_ID, JWT);
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getStatusCodeValue());
  }

  @Test
  public void testCreateStoryUserNotFound() {
    StoryContentDTO storyContentDTO = new StoryContentDTO();
    doThrow(new ResourceNotFoundException(""))
        .when(storyService)
        .create(any(StoryContentDTO.class), anyLong(), anyString());

    ResponseEntity response = storyController.create(storyContentDTO, INVALID_USER_ID, JWT);
    assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCodeValue());
  }

  @Test
  public void testGetStorySuccessful() {
    StoryDTO story = createStoryDto(STORY_ID);

    when(storyService.getStory(anyLong(), anyLong(), anyString())).thenReturn(story);

    ResponseEntity<StoryDTO> response = storyController.getStory(USER_ID, story.getId(), JWT);

    assertEquals(HttpStatus.OK.value(), response.getStatusCodeValue());
    assertEquals("story test", response.getBody().getTitle());
    assertEquals(1L, response.getBody().getId().longValue());
  }

  @Test
  public void testGetStoryUnauthorized() {
    doThrow(new UnauthorizedEventException(""))
        .when(storyService)
        .getStory(anyLong(), anyLong(), anyString());

    ResponseEntity<StoryDTO> response = storyController.getStory(INVALID_USER_ID, STORY_ID, JWT);
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getStatusCodeValue());
  }

  @Test
  public void testGetStoryNotFound() {
    doThrow(new ResourceNotFoundException(""))
        .when(storyService)
        .getStory(anyLong(), anyLong(), anyString());

    ResponseEntity<StoryDTO> response = storyController.getStory(USER_ID, INVALID_STORY_ID, JWT);
    assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCodeValue());
  }

  @Test
  public void testGetStoryListNotEmpty() {
    final int NUMBER_OF_STORIES = 3;
    when(storyService.getStoryList(anyLong(), anyString()))
        .thenReturn(createStoryListDto(NUMBER_OF_STORIES));

    ResponseEntity<StoryListDTO> response = storyController.getStoryList(USER_ID, JWT);
    assertEquals(HttpStatus.OK.value(), response.getStatusCodeValue());
    assertEquals(NUMBER_OF_STORIES, response.getBody().getStories().size());
  }

  @Test
  public void testGetStoryListUnauthorized() {
    doThrow(new UnauthorizedEventException(""))
        .when(storyService)
        .getStoryList(anyLong(), anyString());

    ResponseEntity<StoryListDTO> response = storyController.getStoryList(INVALID_USER_ID, JWT);
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getStatusCodeValue());
  }

  @Test
  public void testGetStoryListEmpty() {
    when(storyService.getStoryList(anyLong(), anyString())).thenReturn(createStoryListDto(0));

    ResponseEntity<StoryListDTO> response = storyController.getStoryList(USER_ID, JWT);
    assertEquals(HttpStatus.OK.value(), response.getStatusCodeValue());
    assertTrue(response.getBody().getStories().isEmpty());
  }

  @Test
  public void testDeleteStorySuccessful() {
    doNothing().when(storyService).deleteStory(anyLong(), anyLong(), anyString());

    ResponseEntity response = storyController.deleteStory(USER_ID, STORY_ID, JWT);
    assertEquals(HttpStatus.OK.value(), response.getStatusCodeValue());
  }

  @Test
  public void testDeleteStoryUnauthorized() {
    doThrow(new UnauthorizedEventException(""))
        .when(storyService)
        .deleteStory(anyLong(), anyLong(), anyString());

    ResponseEntity response = storyController.deleteStory(INVALID_USER_ID, STORY_ID, JWT);
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getStatusCodeValue());
  }

  @Test
  public void testDeleteStoryNotFound() {
    doThrow(new ResourceNotFoundException(""))
        .when(storyService)
        .deleteStory(anyLong(), anyLong(), anyString());

    ResponseEntity response = storyController.deleteStory(USER_ID, INVALID_STORY_ID, JWT);
    assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCodeValue());
  }

  @Test
  public void testModifyStorySuccessful() {
    StoryContentDTO storyContent = new StoryContentDTO("story test", "story content");

    when(storyService.modifyStory(any(StoryContentDTO.class), anyLong(), anyLong(), anyString()))
        .thenReturn(createStoryDto(STORY_ID));
    ResponseEntity<StoryDTO> response =
        storyController.modifyStory(storyContent, USER_ID, STORY_ID, JWT);

    assertEquals(HttpStatus.OK.value(), response.getStatusCodeValue());
    assertEquals(storyContent.getTitle(), response.getBody().getTitle());
    assertEquals(storyContent.getContent(), response.getBody().getContent());
  }

  @Test
  public void testModifyStoryUnauthorized() {
    doThrow(new UnauthorizedEventException(""))
        .when(storyService)
        .modifyStory(any(StoryContentDTO.class), anyLong(), anyLong(), anyString());

    ResponseEntity<StoryDTO> response =
        storyController.modifyStory(new StoryContentDTO(), INVALID_USER_ID, STORY_ID, JWT);

    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getStatusCodeValue());
  }

  @Test
  public void testModifyStoryNotFound() {
    doThrow(new ResourceNotFoundException(""))
        .when(storyService)
        .modifyStory(any(StoryContentDTO.class), anyLong(), anyLong(), anyString());

    ResponseEntity<StoryDTO> response =
        storyController.modifyStory(new StoryContentDTO(), USER_ID, INVALID_STORY_ID, JWT);

    assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCodeValue());
  }

  private StoryDTO createStoryDto(Long storyId) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");
    return new StoryDTO(
        storyId, LocalDateTime.now().format(formatter), "story test", "story content");
  }

  private StoryListDTO createStoryListDto(int numberOfStories) {
    List<StoryDTO> storyDTOS = new ArrayList<>();

    for (long i = 0; i < numberOfStories; i++) {
      storyDTOS.add(createStoryDto(i));
    }
    return new StoryListDTO(storyDTOS);
  }
}
