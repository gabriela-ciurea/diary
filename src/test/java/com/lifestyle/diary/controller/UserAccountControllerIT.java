package com.lifestyle.diary.controller;

import com.lifestyle.diary.dto.CurrentUserInfoDTO;
import com.lifestyle.diary.dto.SignUpDTO;
import com.sun.net.httpserver.Headers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.*;

import static org.junit.Assert.assertEquals;

public class UserAccountControllerIT extends ControllerBaseIT {
  private final String FIRST_NAME = "Adam";
  private final String LAST_NAME = "Smith";
  private final String SIGN_UP_URI = "/api/users";
  private final String CURRENT_USER_INFO_URI = "/api/users/current_info";

  @Before
  public void beforeTest() {
    super.setup();
  }

  @After
  public void afterTest() {
    super.deleteAllTestUsers();
  }

  @Test
  public void testRegisterUserWithValidData() {
    SignUpDTO signUpDTO =
        new SignUpDTO(FIRST_NAME, LAST_NAME, "valid", "valid@email.com", TEST_USER_PASSWORD);

    ResponseEntity<String> response =
        restTemplate.exchange(
            createUrlWithPort(SIGN_UP_URI),
            HttpMethod.POST,
            new HttpEntity<>(signUpDTO),
            String.class);

    assertEquals(HttpStatus.CREATED.value(), response.getStatusCodeValue());
  }

  @Test
  public void testRegisterUserWithUsernameTaken() {
    SignUpDTO signUpDTO =
        new SignUpDTO(
            FIRST_NAME, LAST_NAME, TEST_USER_USERNAME, "another@email.com", TEST_USER_PASSWORD);

    ResponseEntity<String> response =
        restTemplate.exchange(
            createUrlWithPort(SIGN_UP_URI),
            HttpMethod.POST,
            new HttpEntity<>(signUpDTO),
            String.class);

    assertEquals(HttpStatus.FORBIDDEN.value(), response.getStatusCodeValue());
  }

  @Test
  public void testRegisterUserWithEmailTaken() {
    SignUpDTO signUpDTO =
        new SignUpDTO(FIRST_NAME, LAST_NAME, "Erick", TEST_USER_EMAIL, TEST_USER_PASSWORD);

    ResponseEntity<String> response =
        restTemplate.exchange(
            createUrlWithPort(SIGN_UP_URI),
            HttpMethod.POST,
            new HttpEntity<>(signUpDTO),
            String.class);

    assertEquals(HttpStatus.FORBIDDEN.value(), response.getStatusCodeValue());
  }

  @Test
  public void testRegisterUserInvalidUsername() {
    SignUpDTO signUpDTO =
        new SignUpDTO(FIRST_NAME, LAST_NAME, "ad", TEST_USER_EMAIL, TEST_USER_PASSWORD);

    ResponseEntity<String> response =
        restTemplate.exchange(
            createUrlWithPort(SIGN_UP_URI),
            HttpMethod.POST,
            new HttpEntity<>(signUpDTO),
            String.class);

    assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatusCodeValue());
  }

  @Test
  public void testRegisterUserInvalidEmail() {
    SignUpDTO signUpDTO =
        new SignUpDTO(
            FIRST_NAME, LAST_NAME, TEST_USER_USERNAME, "adamsmith.com", TEST_USER_PASSWORD);

    ResponseEntity<String> response =
        restTemplate.exchange(
            createUrlWithPort(SIGN_UP_URI),
            HttpMethod.POST,
            new HttpEntity<>(signUpDTO),
            String.class);

    assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatusCodeValue());
  }

  @Test
  public void testRegisterUserInvalidPassword() {
    SignUpDTO signUpDTO =
        new SignUpDTO(FIRST_NAME, LAST_NAME, TEST_USER_USERNAME, TEST_USER_EMAIL, "");

    ResponseEntity<String> response =
        restTemplate.exchange(
            createUrlWithPort(SIGN_UP_URI),
            HttpMethod.POST,
            new HttpEntity<>(signUpDTO),
            String.class);

    assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatusCodeValue());
  }

  @Test
  public void testGetCurrentUserInfoSuccessful() {
    HttpEntity request = new HttpEntity(getAuthorizationHeaders());

    ResponseEntity<CurrentUserInfoDTO> response =
        restTemplate.exchange(
            createUrlWithPort(CURRENT_USER_INFO_URI),
            HttpMethod.GET,
            request,
            CurrentUserInfoDTO.class);

    assertEquals(HttpStatus.OK.value(), response.getStatusCodeValue());
    assertEquals(TEST_USER_USERNAME, response.getBody().getUsername());
    assertEquals(TEST_USER_EMAIL, response.getBody().getEmail());
  }

  @Test
  public void testGetCurrentUserInfoUnauthorized() {
    Headers headers = new Headers();
    headers.set(HttpHeaders.AUTHORIZATION, "123");
    HttpEntity request = new HttpEntity(headers);

    ResponseEntity<CurrentUserInfoDTO> response =
        restTemplate.exchange(
            createUrlWithPort(CURRENT_USER_INFO_URI),
            HttpMethod.GET,
            request,
            CurrentUserInfoDTO.class);

    assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatusCodeValue());
  }
}
