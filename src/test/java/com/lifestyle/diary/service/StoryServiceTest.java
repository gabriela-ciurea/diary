package com.lifestyle.diary.service;

import com.lifestyle.diary.dto.StoryContentDTO;
import com.lifestyle.diary.dto.StoryDTO;
import com.lifestyle.diary.exception.ResourceNotFoundException;
import com.lifestyle.diary.exception.UnauthorizedEventException;
import com.lifestyle.diary.model.Story;
import com.lifestyle.diary.model.UserAccount;
import com.lifestyle.diary.repository.StoryRepository;
import com.lifestyle.diary.repository.UserRepository;
import com.lifestyle.diary.security.JwtTokenProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StoryServiceTest {
  @MockBean JwtTokenProvider tokenProvider;
  @MockBean UserRepository userRepository;
  @MockBean StoryRepository storyRepository;
  @Autowired StoryService storyService;

  private static final String FIRST_NAME = "Adam";
  private static final String LAST_NAME = "Smith";
  private static final String USERNAME = "adamsmith";
  private static final String EMAIL = "adamsmith@email.com";
  private static final String PASSWORD = "adam123";
  private static final Long VALID_USER_ID = 1L;
  private static final Long NON_EXISTENT_USER_ID = 57L;
  private static final String JWT = "Bearer 123 token";
  private static final UserAccount USER_ACCOUNT =
      new UserAccount(
          VALID_USER_ID, FIRST_NAME, LAST_NAME, USERNAME, EMAIL, PASSWORD, null, new HashSet<>());
  private static final Long STORY_ID = 1L;
  private static final Long INVALID_STORY_ID = 125L;

  @Test
  public void testCreateValidStory() {
    StoryContentDTO storyContentDTO = new StoryContentDTO();

    when(tokenProvider.getUserIdFromJWT(anyString())).thenReturn(VALID_USER_ID);
    when(userRepository.findById(anyLong())).thenReturn(Optional.of(USER_ACCOUNT));
    when(storyRepository.save(any(Story.class))).thenReturn(new Story());
    when(userRepository.save(any(UserAccount.class))).thenReturn(USER_ACCOUNT);

    storyService.create(storyContentDTO, USER_ACCOUNT.getId(), JWT);
  }

  @Test(expected = UnauthorizedEventException.class)
  public void testCreateStoryUnauthorized() {
    when(tokenProvider.getUserIdFromJWT(anyString())).thenReturn(NON_EXISTENT_USER_ID);

    storyService.create(new StoryContentDTO(), USER_ACCOUNT.getId(), JWT);
  }

  @Test(expected = ResourceNotFoundException.class)
  public void testCreateStoryUserNotFound() {
    StoryContentDTO storyContentDTO = new StoryContentDTO();

    when(tokenProvider.getUserIdFromJWT(anyString())).thenReturn(VALID_USER_ID);
    when(userRepository.findById(anyLong())).thenThrow(new ResourceNotFoundException(""));

    storyService.create(storyContentDTO, USER_ACCOUNT.getId(), JWT);
  }

  @Test
  public void testGetStorySuccessful() {
    Story story = createStory(STORY_ID);

    when(tokenProvider.getUserIdFromJWT(anyString())).thenReturn(VALID_USER_ID);
    when(storyRepository.findByIdAndUserAccountId(anyLong(), anyLong()))
        .thenReturn(Optional.of(story));

    StoryDTO storyDTO = storyService.getStory(VALID_USER_ID, STORY_ID, JWT);

    assertEquals(story.getTitle(), storyDTO.getTitle());
    assertEquals(story.getId(), storyDTO.getId());
  }

  @Test(expected = UnauthorizedEventException.class)
  public void testGetStoryUnauthorized() {
    when(tokenProvider.getUserIdFromJWT(anyString())).thenReturn(NON_EXISTENT_USER_ID);
    storyService.getStory(VALID_USER_ID, STORY_ID, JWT);
  }

  @Test(expected = ResourceNotFoundException.class)
  public void testGetStoryNotFound() {
    when(tokenProvider.getUserIdFromJWT(anyString())).thenReturn(VALID_USER_ID);
    storyService.getStory(VALID_USER_ID, INVALID_STORY_ID, JWT);
  }

  @Test
  public void testGetStoryListNotEmpty() {
    final int NUMBER_OF_STORIES = 3;
    when(tokenProvider.getUserIdFromJWT(anyString())).thenReturn(VALID_USER_ID);
    when(storyRepository.findByUserAccountId(anyLong()))
        .thenReturn(createStoryList(NUMBER_OF_STORIES));

    assertEquals(
        NUMBER_OF_STORIES, storyService.getStoryList(VALID_USER_ID, JWT).getStories().size());
  }

  @Test
  public void testGetStoryListEmpty() {
    when(tokenProvider.getUserIdFromJWT(anyString())).thenReturn(VALID_USER_ID);
    when(storyRepository.findByUserAccountId(anyLong())).thenReturn(new ArrayList<>());

    assertTrue(storyService.getStoryList(VALID_USER_ID, JWT).getStories().isEmpty());
  }

  @Test(expected = UnauthorizedEventException.class)
  public void testGetStoryListUnauthorized() {
    when(tokenProvider.getUserIdFromJWT(anyString())).thenReturn(NON_EXISTENT_USER_ID);

    storyService.getStoryList(VALID_USER_ID, JWT);
  }

  @Test
  public void testDeleteStorySuccessful() {
    when(tokenProvider.getUserIdFromJWT(anyString())).thenReturn(VALID_USER_ID);
    when(storyRepository.findByIdAndUserAccountId(anyLong(), anyLong()))
        .thenReturn(Optional.of(createStory(STORY_ID)));
    doNothing().when(storyRepository).delete(any(Story.class));

    storyService.deleteStory(VALID_USER_ID, STORY_ID, JWT);
  }

  @Test(expected = UnauthorizedEventException.class)
  public void testDeleteStoryUnauthorized() {
    when(tokenProvider.getUserIdFromJWT(anyString())).thenReturn(NON_EXISTENT_USER_ID);
    storyService.deleteStory(VALID_USER_ID, STORY_ID, JWT);
  }

  @Test(expected = ResourceNotFoundException.class)
  public void testDeleteStoryNotFound() {
    when(tokenProvider.getUserIdFromJWT(anyString())).thenReturn(VALID_USER_ID);
    when(storyRepository.findByIdAndUserAccountId(anyLong(), anyLong()))
        .thenThrow(new ResourceNotFoundException(""));

    storyService.deleteStory(VALID_USER_ID, STORY_ID, JWT);
  }

  @Test
  public void testModifyStorySuccessful() {
    when(tokenProvider.getUserIdFromJWT(anyString())).thenReturn(VALID_USER_ID);
    when(storyRepository.findByIdAndUserAccountId(anyLong(), anyLong()))
        .thenReturn(Optional.of(createStory(STORY_ID)));

    StoryContentDTO storyContent = new StoryContentDTO("new title", "new content");

    StoryDTO storyDTO = storyService.modifyStory(storyContent, VALID_USER_ID, STORY_ID, JWT);

    assertEquals(storyContent.getTitle(), storyDTO.getTitle());
    assertEquals(storyContent.getContent(), storyDTO.getContent());
  }

  @Test(expected = UnauthorizedEventException.class)
  public void testModifyStoryUnauthorized() {
    when(tokenProvider.getUserIdFromJWT(anyString())).thenReturn(NON_EXISTENT_USER_ID);
    storyService.modifyStory(new StoryContentDTO(), VALID_USER_ID, STORY_ID, JWT);
  }

  @Test(expected = ResourceNotFoundException.class)
  public void testModifyStoryNotFound() {
    when(tokenProvider.getUserIdFromJWT(anyString())).thenReturn(VALID_USER_ID);
    when(storyRepository.findByIdAndUserAccountId(anyLong(), anyLong()))
        .thenThrow(new ResourceNotFoundException(""));

    storyService.modifyStory(new StoryContentDTO(), VALID_USER_ID, STORY_ID, JWT);
  }

  private Story createStory(Long storyId) {
    UserAccount userAccount = new UserAccount();
    userAccount.setId(VALID_USER_ID);
    return new Story(storyId, LocalDateTime.now(), "story test", "story content", userAccount);
  }

  private List<Story> createStoryList(int numberOfStories) {
    List<Story> stories = new ArrayList<>();

    for (long i = 0; i < numberOfStories; i++) {
      stories.add(createStory(i));
    }

    return stories;
  }
}
