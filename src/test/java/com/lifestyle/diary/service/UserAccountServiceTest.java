package com.lifestyle.diary.service;

import com.lifestyle.diary.dto.SignUpDTO;
import com.lifestyle.diary.exception.*;
import com.lifestyle.diary.model.Role;
import com.lifestyle.diary.model.RoleName;
import com.lifestyle.diary.model.UserAccount;
import com.lifestyle.diary.repository.RoleRepository;
import com.lifestyle.diary.repository.UserRepository;
import com.lifestyle.diary.security.JwtTokenProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserAccountServiceTest {
  @MockBean UserRepository userRepository;
  @MockBean RoleRepository roleRepository;
  @MockBean JwtTokenProvider tokenProvider;
  @Autowired UserAccountService userAccountService;

  private static final Long VALID_USER_ID = 1L;
  private static final String FIRST_NAME = "Adam";
  private static final String LAST_NAME = "Smith";
  private static final String USERNAME = "adamsmith";
  private static final String EMAIL = "adamsmith@email.com";
  private static final String PASSWORD = "adam123";
  private static final String JWT = "Bearer 123 token";

  @Test
  public void testRegisterUserWithValidData() {
    UserAccount userAccount = new UserAccount(FIRST_NAME, LAST_NAME, USERNAME, EMAIL, PASSWORD);
    SignUpDTO signUpDTO =
        new SignUpDTO(
            userAccount.getFirstName(),
            userAccount.getLastName(),
            userAccount.getUsername(),
            userAccount.getEmail(),
            userAccount.getPassword());

    when(userRepository.existsByUsername(anyString())).thenReturn(false);
    when(userRepository.existsByEmail(anyString())).thenReturn(false);
    when(roleRepository.findByName(RoleName.ROLE_USER))
        .thenReturn(Optional.of(new Role(RoleName.ROLE_USER)));
    when(userRepository.save(any(UserAccount.class))).thenReturn(userAccount);

    assertEquals(USERNAME, userAccountService.registerUser(signUpDTO).getUsername());
    assertEquals(EMAIL, userAccountService.registerUser(signUpDTO).getEmail());
  }

  @Test(expected = UsernameAlreadyUsedException.class)
  public void testRegisterUserWithUsernameTaken() {
    SignUpDTO signUpDTO = new SignUpDTO(FIRST_NAME, LAST_NAME, USERNAME, EMAIL, PASSWORD);
    when(userRepository.existsByUsername(anyString())).thenReturn(true);

    userAccountService.registerUser(signUpDTO);
  }

  @Test(expected = EmailAlreadyUsedException.class)
  public void testRegisterUserWithEmailTaken() {
    SignUpDTO signUpDTO = new SignUpDTO(FIRST_NAME, LAST_NAME, USERNAME, EMAIL, PASSWORD);

    when(userRepository.existsByUsername(anyString())).thenReturn(false);
    when(userRepository.existsByEmail(anyString())).thenReturn(true);

    userAccountService.registerUser(signUpDTO);
  }

  @Test(expected = AppException.class)
  public void testRegisterUserWithMissingRoleInDB() {
    SignUpDTO signUpDTO = new SignUpDTO(FIRST_NAME, LAST_NAME, USERNAME, EMAIL, PASSWORD);

    when(userRepository.existsByUsername(anyString())).thenReturn(false);
    when(userRepository.existsByEmail(anyString())).thenReturn(false);
    when(roleRepository.findByName(RoleName.ROLE_USER)).thenThrow(AppException.class);

    userAccountService.registerUser(signUpDTO);
  }

  @Test
  public void testGetCurrentUserInfoSuccessful() {
    UserAccount userAccount = new UserAccount(FIRST_NAME, LAST_NAME, USERNAME, EMAIL, PASSWORD);

    when(tokenProvider.getUserIdFromJWT(anyString())).thenReturn(VALID_USER_ID);
    when(userRepository.findById(anyLong())).thenReturn(Optional.of(userAccount));

    userAccountService.getCurrentUserInfo(JWT);

    assertEquals(FIRST_NAME, userAccount.getFirstName());
    assertEquals(USERNAME, userAccount.getUsername());
    assertEquals(EMAIL, userAccount.getEmail());
  }

  @Test(expected = UnauthorizedEventException.class)
  public void testGetCurrentUserInfoUnauthorized() {
    when(tokenProvider.getUserIdFromJWT(anyString())).thenThrow(new UnauthorizedEventException(""));

    userAccountService.getCurrentUserInfo(JWT);
  }

  @Test(expected = ResourceNotFoundException.class)
  public void testGetCurrentUserInfoNotFound() {
    when(tokenProvider.getUserIdFromJWT(anyString())).thenReturn(VALID_USER_ID);
    when(userRepository.findById(anyLong())).thenThrow(new ResourceNotFoundException(""));

    userAccountService.getCurrentUserInfo(JWT);
  }
}
