-- noinspection SqlNoDataSourceInspectionForFile

DROP TABLE USER_ACCOUNT_ROLE;
DROP TABLE STORY;
DROP TABLE USER_ACCOUNT;
DROP TABLE ROLE;

CREATE TABLE USER_ACCOUNT(
ID BIGSERIAL PRIMARY KEY,
FIRST_NAME VARCHAR(50) NOT NULL,
LAST_NAME VARCHAR(50) NOT NULL,
USERNAME VARCHAR(20) NOT NULL,
EMAIL VARCHAR(40) NOT NULL,
PASSWORD VARCHAR(255) NOT NULL
);

CREATE TABLE ROLE(
ID BIGSERIAL PRIMARY KEY,
NAME VARCHAR(100) NOT NULL
);

CREATE TABLE USER_ACCOUNT_ROLE(
ID BIGSERIAL PRIMARY KEY,
USER_ACCOUNT_ID BIGINT REFERENCES USER_ACCOUNT(ID),
ROLE_ID BIGINT REFERENCES ROLE(ID)
);

CREATE TABLE STORY(
ID BIGSERIAL PRIMARY KEY,
CREATION_DATE TIMESTAMP NOT NULL,
TITLE VARCHAR(100),
CONTENT TEXT,
USER_ACCOUNT_ID BIGINT REFERENCES USER_ACCOUNT(ID)
);

INSERT INTO ROLE(name) VALUES('ROLE_USER');
