package com.lifestyle.diary.security;

import com.lifestyle.diary.model.UserAccount;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@EqualsAndHashCode
public class UserPrincipal implements UserDetails {
  private Long id;
  private String firstName;
  private String lastName;
  private String username;
  private String email;
  private String password;
  private Collection<? extends GrantedAuthority> authorities;

  private UserPrincipal(
      Long id,
      String firstName,
      String lastName,
      String username,
      String email,
      String password,
      Collection<? extends GrantedAuthority> authorities) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.username = username;
    this.email = email;
    this.password = password;
    this.authorities = authorities;
  }

  public static UserPrincipal create(UserAccount userAccount) {
    List<GrantedAuthority> authorities =
        userAccount.getRoles().stream()
            .map(role -> new SimpleGrantedAuthority(role.getName().name()))
            .collect(Collectors.toList());

    return new UserPrincipal(
        userAccount.getId(),
        userAccount.getFirstName(),
        userAccount.getLastName(),
        userAccount.getUsername(),
        userAccount.getEmail(),
        userAccount.getPassword(),
        authorities);
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return authorities;
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }
}
