package com.lifestyle.diary.security;

import com.lifestyle.diary.model.UserAccount;
import com.lifestyle.diary.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

  private UserRepository userRepository;

  @Autowired
  public CustomUserDetailsService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  @Transactional
  public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {
    UserAccount userAccount =
        userRepository
            .findByUsernameOrEmail(usernameOrEmail, usernameOrEmail)
            .orElseThrow(
                () ->
                    new UsernameNotFoundException(
                        "UserAccount not found with username or email : " + usernameOrEmail));

    return UserPrincipal.create(userAccount);
  }

  @Transactional
  public UserDetails loadUserById(Long id) {
    UserAccount userAccount =
        userRepository
            .findById(id)
            .orElseThrow(() -> new UsernameNotFoundException("UserAccount not found with id : " + id));

    return UserPrincipal.create(userAccount);
  }
}
