package com.lifestyle.diary.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Entity
@Table(name = "story")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Story {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotNull private LocalDateTime creationDate;

  @Size(max = 100)
  private String title;

  private String content;

  @ManyToOne(fetch = FetchType.LAZY)
  private UserAccount userAccount;

  public Story(LocalDateTime creationDate, String title, String content, UserAccount userAccount) {
    this.creationDate = creationDate;
    this.title = title;
    this.content = content;
    this.userAccount = userAccount;
  }
}
