package com.lifestyle.diary.service;

import com.lifestyle.diary.dto.CurrentUserInfoDTO;
import com.lifestyle.diary.dto.LoginDTO;
import com.lifestyle.diary.dto.SignUpDTO;
import com.lifestyle.diary.exception.*;
import com.lifestyle.diary.model.Role;
import com.lifestyle.diary.model.RoleName;
import com.lifestyle.diary.model.UserAccount;
import com.lifestyle.diary.repository.RoleRepository;
import com.lifestyle.diary.repository.UserRepository;
import com.lifestyle.diary.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class UserAccountService {

  private AuthenticationManager authenticationManager;
  private UserRepository userRepository;
  private RoleRepository roleRepository;
  private PasswordEncoder passwordEncoder;
  private JwtTokenProvider tokenProvider;

  @Autowired
  public UserAccountService(
      AuthenticationManager authenticationManager,
      UserRepository userRepository,
      RoleRepository roleRepository,
      PasswordEncoder passwordEncoder,
      JwtTokenProvider tokenProvider) {
    this.authenticationManager = authenticationManager;
    this.userRepository = userRepository;
    this.roleRepository = roleRepository;
    this.passwordEncoder = passwordEncoder;
    this.tokenProvider = tokenProvider;
  }

  public String authenticate(LoginDTO loginDTO) {
    Authentication authentication =
        authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(
                loginDTO.getUsernameOrEmail(), loginDTO.getPassword()));

    SecurityContextHolder.getContext().setAuthentication(authentication);

    return tokenProvider.generateToken(authentication);
  }

  public UserAccount registerUser(SignUpDTO signUpDTO)
      throws UsernameAlreadyUsedException, EmailAlreadyUsedException, AppException {
    if (userRepository.existsByUsername(signUpDTO.getUsername())) {
      throw new UsernameAlreadyUsedException("Username already in use!");
    }

    if (userRepository.existsByEmail(signUpDTO.getEmail())) {
      throw new EmailAlreadyUsedException("Email Address already in use!");
    }

    UserAccount userAccount =
        new UserAccount(
            signUpDTO.getFirstName(),
            signUpDTO.getLastName(),
            signUpDTO.getUsername(),
            signUpDTO.getEmail(),
            signUpDTO.getPassword());
    userAccount.setPassword(passwordEncoder.encode(userAccount.getPassword()));
    Role userRole =
        roleRepository
            .findByName(RoleName.ROLE_USER)
            .orElseThrow(() -> new AppException("UserAccount Role not set."));
    userAccount.setRoles(Collections.singleton(userRole));

    return userRepository.save(userAccount);
  }

  public CurrentUserInfoDTO getCurrentUserInfo(String jwt) {
    Long userId;

    try {
      userId = tokenProvider.getUserIdFromJWT(jwt);
    } catch (Exception e) {
      throw new UnauthorizedEventException("Invalid jwt");
    }

    UserAccount userAccount =
        userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException(""));

    return new CurrentUserInfoDTO(
        userAccount.getId(),
        userAccount.getFirstName(),
        userAccount.getLastName(),
        userAccount.getUsername(),
        userAccount.getEmail());
  }
}
