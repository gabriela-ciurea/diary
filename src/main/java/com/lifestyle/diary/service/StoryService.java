package com.lifestyle.diary.service;

import com.lifestyle.diary.dto.StoryContentDTO;
import com.lifestyle.diary.dto.StoryDTO;
import com.lifestyle.diary.dto.StoryListDTO;
import com.lifestyle.diary.exception.ResourceNotFoundException;
import com.lifestyle.diary.exception.UnauthorizedEventException;
import com.lifestyle.diary.model.Story;
import com.lifestyle.diary.model.UserAccount;
import com.lifestyle.diary.repository.StoryRepository;
import com.lifestyle.diary.repository.UserRepository;
import com.lifestyle.diary.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class StoryService {
  private StoryRepository storyRepository;
  private UserRepository userRepository;
  private JwtTokenProvider tokenProvider;
  private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");

  @Autowired
  public StoryService(
      StoryRepository storyRepository,
      UserRepository userRepository,
      JwtTokenProvider tokenProvider) {
    this.storyRepository = storyRepository;
    this.userRepository = userRepository;
    this.tokenProvider = tokenProvider;
  }

  public void create(StoryContentDTO storyContentDTO, Long userId, String jwt) {
    checkUserIdInJwt(userId, jwt, "Cannot create story for userAccount id");

    UserAccount userAccount =
        userRepository
            .findById(userId)
            .orElseThrow(
                () -> new ResourceNotFoundException("UserAccount not found with id: " + userId));
    Story story =
        storyRepository.save(
            new Story(
                LocalDateTime.now(),
                storyContentDTO.getTitle(),
                storyContentDTO.getContent(),
                userAccount));

    userAccount.getStories().add(story);
    userRepository.save(userAccount);
  }

  public StoryDTO getStory(Long userId, Long storyId, String jwt) {
    checkUserIdInJwt(userId, jwt, "Cannot find story for userAccount id");

    Story story =
        storyRepository
            .findByIdAndUserAccountId(storyId, userId)
            .orElseThrow(() -> new ResourceNotFoundException("Story not found!"));

    return new StoryDTO(
        story.getId(),
        story.getCreationDate().format(formatter),
        story.getTitle(),
        story.getContent());
  }

  public StoryListDTO getStoryList(Long userId, String jwt) {
    checkUserIdInJwt(userId, jwt, "Cannot find stories for userAccount id");

    List<Story> stories = storyRepository.findByUserAccountId(userId);

    List<StoryDTO> storyDtoList = new ArrayList<>();

    for (Story story : stories) {
      storyDtoList.add(
          new StoryDTO(
              story.getId(),
              story.getCreationDate().format(formatter),
              story.getTitle(),
              story.getContent()));
    }

    return new StoryListDTO(storyDtoList);
  }

  public void deleteStory(Long userId, Long storyId, String jwt) {
    checkUserIdInJwt(userId, jwt, "Story not found for userAccount id");

    Story story =
        storyRepository
            .findByIdAndUserAccountId(storyId, userId)
            .orElseThrow(() -> new ResourceNotFoundException("Story not found!"));

    storyRepository.delete(story);
  }

  public StoryDTO modifyStory(StoryContentDTO storyContent, Long userId, Long storyId, String jwt) {
    checkUserIdInJwt(userId, jwt, "Cannot find story for userAccount id");

    Story story =
        storyRepository
            .findByIdAndUserAccountId(storyId, userId)
            .orElseThrow(() -> new ResourceNotFoundException("Story not found!"));

    story.setTitle(storyContent.getTitle());
    story.setContent(storyContent.getContent());

    storyRepository.save(story);

    return new StoryDTO(
        story.getId(),
        story.getCreationDate().format(formatter),
        story.getTitle(),
        story.getContent());
  }

  private void checkUserIdInJwt(Long userId, String jwt, String exceptionMessage) {
    if (userId.longValue() != tokenProvider.getUserIdFromJWT(jwt).longValue()) {
      throw new UnauthorizedEventException(exceptionMessage + ": " + userId);
    }
  }
}
