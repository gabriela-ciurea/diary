package com.lifestyle.diary.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "CurrentUserInfo", description = "All the info about the current logged user")
public class CurrentUserInfoDTO {
  @ApiModelProperty(notes = "The database generated user ID")
  private Long id;

  @ApiModelProperty(notes = "The user first name")
  private String firstName;

  @ApiModelProperty(notes = "The user last name")
  private String lastName;

  @ApiModelProperty(notes = "The username of the user")
  private String username;

  @ApiModelProperty(notes = "The user email")
  private String email;
}
