package com.lifestyle.diary.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "StoryContent", description = "The content of a story")
public class StoryContentDTO {
  @Size(max = 100)
  @ApiModelProperty(notes = "The title of a story")
  private String title;

  @Size(max = 3000)
  @ApiModelProperty(notes = "The content of a story")
  private String content;
}
