package com.lifestyle.diary.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "Login", description = "All the details about the login")
public class LoginDTO {
  @NotBlank
  @ApiModelProperty(notes = "Valid username or email")
  private String usernameOrEmail;

  @NotBlank
  @Size(min = 3, max = 20)
  @ApiModelProperty(notes = "Valid password")
  private String password;
}
