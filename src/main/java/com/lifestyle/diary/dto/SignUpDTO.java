package com.lifestyle.diary.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "SignUp", description = "All user info required to sign up")
public class SignUpDTO {
  @NotBlank
  @Size(min = 4, max = 50)
  @ApiModelProperty(notes = "The user first name")
  private String firstName;

  @NotBlank
  @Size(min = 4, max = 50)
  @ApiModelProperty(notes = "The user lst name")
  private String lastName;

  @NotBlank
  @Size(min = 3, max = 20)
  @ApiModelProperty(notes = "The username for the user")
  private String username;

  @NotBlank
  @Size(max = 40)
  @Email
  @ApiModelProperty(notes = "The user email")
  private String email;

  @NotBlank
  @Size(min = 6, max = 20)
  @ApiModelProperty(notes = "The user password")
  private String password;
}
