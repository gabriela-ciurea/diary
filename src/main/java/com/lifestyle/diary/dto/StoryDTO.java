package com.lifestyle.diary.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Story", description = "All details about the story")
public class StoryDTO {
  @ApiModelProperty(notes = "The database generated story ID")
  private Long id;

  @ApiModelProperty(notes = "The date of a story")
  private String creationDate;

  @ApiModelProperty(notes = "The title of a story")
  private String title;

  @ApiModelProperty(notes = "The content of a story")
  private String content;
}
