package com.lifestyle.diary.exception;

public class UsernameAlreadyUsedException extends RuntimeException {
  public UsernameAlreadyUsedException(String message) {
    super(message);
  }
}
