package com.lifestyle.diary.exception;

public class UnauthorizedEventException extends RuntimeException {
    public UnauthorizedEventException(String message) {
        super(message);
    }
}
