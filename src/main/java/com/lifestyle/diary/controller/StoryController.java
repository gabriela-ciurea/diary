package com.lifestyle.diary.controller;

import com.lifestyle.diary.dto.StoryContentDTO;
import com.lifestyle.diary.dto.StoryDTO;
import com.lifestyle.diary.dto.StoryListDTO;
import com.lifestyle.diary.exception.ResourceNotFoundException;
import com.lifestyle.diary.exception.UnauthorizedEventException;
import com.lifestyle.diary.service.StoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api/users")
@RestController
@Api(value = "Story endpoints")
public class StoryController {
  private StoryService storyService;

  @Autowired
  public StoryController(StoryService storyService) {
    this.storyService = storyService;
  }

  @ApiOperation(value = "Create a new story")
  @PostMapping("/{id}/stories")
  public ResponseEntity create(
      @Valid @RequestBody StoryContentDTO story,
      @PathVariable("id") Long userId,
      @RequestHeader(value = HttpHeaders.AUTHORIZATION) String jwt) {

    try {
      storyService.create(story, userId, jwt);
    } catch (UnauthorizedEventException e) {
      return new ResponseEntity(HttpStatus.UNAUTHORIZED);
    } catch (ResourceNotFoundException e) {
      return new ResponseEntity(HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity(HttpStatus.CREATED);
  }

  @ApiOperation(value = "Get an existing story by user ID")
  @GetMapping("/{id}/stories/{storyId}")
  public ResponseEntity<StoryDTO> getStory(
      @PathVariable("id") Long userId,
      @PathVariable("storyId") Long storyId,
      @RequestHeader(value = HttpHeaders.AUTHORIZATION) String jwt) {

    StoryDTO story;

    try {
      story = storyService.getStory(userId, storyId, jwt);
    } catch (UnauthorizedEventException e) {
      return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    } catch (ResourceNotFoundException e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(story, HttpStatus.OK);
  }

  @ApiOperation(value = "Get all existing stories by user ID")
  @GetMapping("/{id}/stories")
  public ResponseEntity<StoryListDTO> getStoryList(
      @PathVariable("id") Long userId,
      @RequestHeader(value = HttpHeaders.AUTHORIZATION) String jwt) {

    try {
      return new ResponseEntity<>(storyService.getStoryList(userId, jwt), HttpStatus.OK);
    } catch (UnauthorizedEventException e) {
      return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
  }

  @ApiOperation(value = "Delete an existing story by story ID")
  @DeleteMapping("/{id}/stories/{storyId}")
  public ResponseEntity deleteStory(
      @PathVariable("id") Long userId,
      @PathVariable("storyId") Long storyId,
      @RequestHeader(value = HttpHeaders.AUTHORIZATION) String jwt) {

    try {
      storyService.deleteStory(userId, storyId, jwt);
      return new ResponseEntity(HttpStatus.OK);
    } catch (UnauthorizedEventException e) {
      return new ResponseEntity(HttpStatus.UNAUTHORIZED);
    } catch (ResourceNotFoundException e) {
      return new ResponseEntity(HttpStatus.NOT_FOUND);
    }
  }

  @ApiOperation(value = "Modify an existing story by story ID")
  @PutMapping("/{id}/stories/{storyId}")
  public ResponseEntity<StoryDTO> modifyStory(
      @Valid @RequestBody StoryContentDTO storyContent,
      @PathVariable("id") Long userId,
      @PathVariable("storyId") Long storyId,
      @RequestHeader(value = HttpHeaders.AUTHORIZATION) String jwt) {

    try {
      return new ResponseEntity<>(
          storyService.modifyStory(storyContent, userId, storyId, jwt), HttpStatus.OK);
    } catch (UnauthorizedEventException e) {
      return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    } catch (ResourceNotFoundException e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }
}
