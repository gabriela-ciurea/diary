package com.lifestyle.diary.controller;

import com.lifestyle.diary.dto.CurrentUserInfoDTO;
import com.lifestyle.diary.dto.SignUpDTO;
import com.lifestyle.diary.exception.*;
import com.lifestyle.diary.model.UserAccount;
import com.lifestyle.diary.service.UserAccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RequestMapping("/api/users")
@RestController
@Api(value = "User endpoints")
public class UserAccountController {
  private UserAccountService userAccountService;

  @Autowired
  public UserAccountController(UserAccountService userAccountService) {
    this.userAccountService = userAccountService;
  }

  @ApiOperation(value = "Create a new user")
  @PostMapping
  public ResponseEntity registerUser(@Valid @RequestBody SignUpDTO signUpDTO) {
    UserAccount userAccount;
    try {
      userAccount = userAccountService.registerUser(signUpDTO);
    } catch (UsernameAlreadyUsedException | EmailAlreadyUsedException e) {
      return new ResponseEntity(HttpStatus.FORBIDDEN);
    } catch (AppException e) {
      return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    URI location =
        ServletUriComponentsBuilder.fromCurrentContextPath()
            .path("/api/users/{username}")
            .buildAndExpand(userAccount.getUsername())
            .toUri();

    return ResponseEntity.created(location).body(HttpStatus.OK);
  }

  @ApiOperation(value = "Get information for current logged user")
  @GetMapping("/current_info")
  public ResponseEntity<CurrentUserInfoDTO> getCurrentUserInfo(
      @RequestHeader(value = HttpHeaders.AUTHORIZATION) String jwt) {

    try {
      return new ResponseEntity<>(userAccountService.getCurrentUserInfo(jwt), HttpStatus.OK);
    } catch (UnauthorizedEventException e) {
      return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    } catch (ResourceNotFoundException e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }
}
