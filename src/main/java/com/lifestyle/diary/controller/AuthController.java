package com.lifestyle.diary.controller;

import com.lifestyle.diary.dto.LoginDTO;
import com.lifestyle.diary.service.UserAccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RequestMapping("/api/auth")
@RestController
@Api(value = "Authentication endpoints")
public class AuthController {

  private UserAccountService userAccountService;

  @Autowired
  public AuthController(UserAccountService userAccountService) {
    this.userAccountService = userAccountService;
  }

  @ApiOperation(value = "Sign in user and return JWT on headers")
  @PostMapping("/signin")
  public ResponseEntity authenticateUser(@Valid @RequestBody LoginDTO loginDTO) {
    String jwt = userAccountService.authenticate(loginDTO);
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.add(HttpHeaders.AUTHORIZATION, "Bearer " + jwt);

    return new ResponseEntity<>(httpHeaders, HttpStatus.OK);
  }
}
