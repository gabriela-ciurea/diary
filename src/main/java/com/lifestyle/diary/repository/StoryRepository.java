package com.lifestyle.diary.repository;

import com.lifestyle.diary.model.Story;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface StoryRepository extends JpaRepository<Story, Long> {
  List<Story> findByUserAccountId(Long id);

  Optional<Story> findById(Long storyId);

  Optional<Story> findByIdAndUserAccountId(Long storyId, Long userAccountId);
}
